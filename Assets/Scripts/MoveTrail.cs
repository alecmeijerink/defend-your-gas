﻿using UnityEngine;
using System.Collections;

public class MoveTrail : MonoBehaviour {
	
	public int moveSpeed = 230;
	public LineRenderer trail;

	void Start(){
		trail = GetComponent<LineRenderer>();
		trail.sortingLayerName = "Default";
		trail.sortingOrder = 20;
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.right * Time.deltaTime * moveSpeed);
		Destroy (gameObject, 1);
	}
}