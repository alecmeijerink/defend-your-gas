﻿using UnityEngine;
using System.Collections;

public class BackgroundParalax : MonoBehaviour {

	public Transform target;
	public float dampening;
	public float zLayer = 10;
	public float yLayer = 0;

	// Use this for initialization
	void Start () {
		transform.position = new Vector3((target.position.x * dampening), yLayer, zLayer);
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3((target.position.x * dampening), yLayer, zLayer);
	}
}
