﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	//public Transform rotateable;
	public float fireRate = 0;
	public int Damage = 10;
	public LayerMask whatToHit;
	public GameObject gunHolder;

	public Transform BulletTrailPrefab;
	public float effectSpawnRate = 0;

	private float timeToFire = 0;
	private float timeToSpawnEffect;
	private float rotationCorrection = 0;
	private Transform firePoint;
	//private Transform m_Gun;

	// Use this for initialization
	void Awake () {
		firePoint = transform.FindChild("FirePoint");
		//m_Gun = Character.transform.Find("GunHolder");

		if (firePoint == null) {
			Debug.LogError("FirePoint not found");
		}
	}
	
	// Update is called once per frame
	void Update () {
		// Aim gun
		//this.transform.rotation = quanternion.lookat(mouseposition in world space)

		if (fireRate == 0) {
			if (Input.GetButtonDown ("Fire1")) {
				Shoot ();
			}
		} else {
			if (Input.GetButton ("Fire1") && Time.time > timeToFire) {
				timeToFire = Time.time + 1/fireRate;
				Shoot ();
			}
		}
	}

	void Shoot(){
		//Debug.Log ("Fire!");
		//Vector2 mousePosition = new Vector2 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y);
		Vector2 aimPosition = new Vector2 (GameObject.Find("Aim").transform.position.x, GameObject.Find("Aim").transform.position.y);
		Vector2 firePointposition = new Vector2 (firePoint.position.x, firePoint.position.y);
		//Vector2 firePointrotation = Vector3.forward;

		if (Time.time >= timeToSpawnEffect) {
			Effect();
			timeToSpawnEffect = Time.time + 1 / effectSpawnRate;
		}

		RaycastHit2D hit = Physics2D.Raycast (firePointposition, aimPosition - firePointposition, 100, whatToHit);
		Debug.DrawLine (firePointposition, (aimPosition-firePointposition)*100, Color.cyan);
		if (hit.collider != null) {
			Debug.DrawLine(firePointposition, hit.point, Color.red);

			Enemy enemy = hit.collider.GetComponent<Enemy>();
			if(enemy != null){
				//Debug.Log("We hit " + hit.collider.name + " and did " + Damage + " damage.");
				enemy.DamageEnemy(Damage);
			}
		}
	}

	void Effect(){
		if (gunHolder.transform.localScale.x == 1) {
			rotationCorrection = 0;
		} else {
			rotationCorrection = 180;
		}
		Quaternion gunRotation = Quaternion.Euler(new Vector3(0,0,(firePoint.rotation.eulerAngles.z * gunHolder.transform.localScale.x) + rotationCorrection));
		//Debug.Log (gunRotation);
		Transform trail = Instantiate (BulletTrailPrefab, firePoint.position, gunRotation) as Transform; //firePoint.rotation
		//Transform clone = Instantiate
		//Destroy (trail.gameObject, 0.02f);
	}

	public Vector2 Vector2FromAngle(float a){
		a *= Mathf.Deg2Rad;
		return new Vector2(Mathf.Cos(a), Mathf.Sin(a));
	}
}
