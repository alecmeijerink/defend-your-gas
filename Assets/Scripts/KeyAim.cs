﻿using UnityEngine;
using System.Collections;

public class KeyAim : MonoBehaviour {

	public float RotateSpeed = 4;
	public float smooth = 2.0F;

	// Use this for initialization
	void Start () {
		transform.rotation = Quaternion.identity;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void FixedUpdate()
	{
		// Read the inputs.
		bool up = Input.GetKey("up");
		bool down = Input.GetKey("down");
		
		// Pass all parameters to the character control script.
		Aim(up, down);
	}

	public void Aim(bool up, bool down){
		if (up || down && up != down) {
			//float Rotation = transform.rotation;
			float Rotation = 0;
			if(up){
				Rotation = RotateSpeed;
			} else if (down){
				Rotation = RotateSpeed * -1;
			}
			transform.Rotate (Vector3.forward * Rotation);
		}
	}
}
