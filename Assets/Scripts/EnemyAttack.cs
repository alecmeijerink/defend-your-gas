﻿using UnityEngine;
using System.Collections;


public class EnemyAttack : MonoBehaviour
{
	public float timeBetweenAttacks = 0.5f;     // The time in seconds between each attack.
	public int attackDamage = 10;               // The amount of health taken away per attack.
	
	Animator anim;                              // Reference to the animator component.
	GameObject attackPoint;                     // Reference to the player GameObject.
	BuildingHealth buildingHealth;              // Reference to the player's health.
	Enemy Enemy;                    			// Reference to this enemy's health.
	bool buildingInRange;                         // Whether player is within the trigger collider and can be attacked.
	float timer;                                // Timer for counting up to the next attack.
	
	
	void Awake ()
	{
		// Setting up the references.
		attackPoint = GameObject.FindGameObjectWithTag ("AttackPoint");
		buildingHealth = attackPoint.GetComponent <BuildingHealth> ();
		Enemy = GetComponent<Enemy>();
		anim = GetComponent <Animator> ();
	}
	
	
	void OnTriggerEnter2D (Collider2D other)
	{
		// If the entering collider is the player...
		if(other.gameObject == attackPoint)
		{
			// ... the player is in range.
			//Debug.Log ("AttackPoint in range of enemy.");
			buildingInRange = true;
			anim.SetBool("Attack", true);
		}
	}
	
	
	void OnTriggerExit2D (Collider2D other)
	{
		// If the exiting collider is the player...
		if(other.gameObject == attackPoint)
		{
			// ... the player is no longer in range.
			buildingInRange = false;
			anim.SetBool("Attack", false);
		}
	}
	
	
	void Update ()
	{
		// Add the time since Update was last called to the timer.
		timer += Time.deltaTime;
		
		// If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
		if(timer >= timeBetweenAttacks && buildingInRange && Enemy.stats.Health > 0)
		{
			// ... attack.
			Attack ();
		}
		
		// If the player has zero or less health...
		if(buildingHealth.currentHealth <= 0)
		{
			// ... tell the animator the player is dead.
			//anim.SetTrigger ("PlayerDead");
		}
	}
	
	
	void Attack ()
	{
		// Reset the timer.
		timer = 0f;
		
		// If the player has health to lose...
		if(buildingHealth.currentHealth > 0)
		{
			// ... damage the player.
			buildingHealth.TakeDamage (attackDamage);
		}
	}
}