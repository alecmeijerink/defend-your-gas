using System;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class PlatformerCharacter2D : MonoBehaviour
    {
        [SerializeField] private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis..
        [SerializeField] private LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character

        private Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
		//private Transform m_CeilingCheck;   // A position marking where to check for ceilings
		private Transform m_Gun;			// Get gun as transform so we can mirror it

        const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
        const float k_CeilingRadius = .01f; // Radius of the overlap circle to determine if the player can stand up
		private bool m_Grounded;            // Whether or not the player is grounded.
        private Animator m_Anim;            // Reference to the player's animator component.
        private Rigidbody2D m_Rigidbody2D;
        private bool m_FacingRight = true;  // For determining which way the player is currently facing.

        private void Awake()
        {
            // Setting up references.
            m_GroundCheck = transform.Find("GroundCheck");
			//m_CeilingCheck = transform.Find("CeilingCheck");
			m_Gun = transform.Find("GunHolder");
            m_Anim = GetComponent<Animator>();
            m_Rigidbody2D = GetComponent<Rigidbody2D>();
        }


        private void FixedUpdate()
        {
            m_Grounded = false;

            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            // This can be done using layers instead but Sample Assets will not overwrite your project settings.
            Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                    m_Grounded = true;
            }
            m_Anim.SetBool("Ground", m_Grounded);

            // Set the vertical animation
            m_Anim.SetFloat("vSpeed", m_Rigidbody2D.velocity.y);
        }


        public void Move(float move, bool left, bool right, bool up, bool down)
        {
            // Set whether or not the character is crouching in the animator
            //m_Anim.SetBool("Crouch", crouch);

			//m_Anim.SetFloat ("Speed", Mathf.Abs (move));

            //only control the player if moving left or right, not both
			//Debug.Log (move);
			m_Anim.SetFloat("Move", move);
			m_Rigidbody2D.velocity = new Vector2 (move * m_MaxSpeed, m_Rigidbody2D.velocity.y);
			if (move < 0 && m_FacingRight) {
				Flip();
			} else if (move > 0 && !m_FacingRight){
				Flip();
			}
        }


        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
			Vector3 theScale = m_Gun.transform.localScale;
			theScale.x *= -1;
			m_Gun.transform.localScale = theScale;
        }
    }
}
