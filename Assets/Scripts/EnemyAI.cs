﻿using UnityEngine;
using Pathfinding;
using System.Collections;

[RequireComponent (typeof (Rigidbody2D))]
[RequireComponent (typeof (Seeker))]

public class EnemyAI : MonoBehaviour {

	public Transform target;
	public float updateRate = 2f;

	private Seeker seeker;
	private Rigidbody2D rb;

	public Path path;

	public float speed = 300f;
	public ForceMode2D fMode;

	[HideInInspector]
	public bool PathIsEnded = false;
	public float NextWaypointDistance = 3;

	private int currentWaypoint = 0;

	private Animator m_Anim;

	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectWithTag ("AttackPoint").transform;
		seeker = GetComponent<Seeker> ();
		rb = GetComponent<Rigidbody2D> ();
		m_Anim = GetComponent<Animator>();

		if (target == null) {
			Debug.LogError("No attackpoint found");
			return;
		}

		seeker.StartPath (transform.position, target.position, OnPathComplete);

		StartCoroutine (UpdatePath ());
	}

	IEnumerator UpdatePath(){
		if (target == null) {
			return false;
		}

		seeker.StartPath (transform.position, target.position, OnPathComplete);

		yield return new WaitForSeconds (1f / updateRate);
		StartCoroutine (UpdatePath ());
	}

	public void OnPathComplete(Path p){
		//Debug.Log ("Did it have an error " + p.error);
		if (!p.error) {
			path = p;
			currentWaypoint = 0;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (target == null) {
			return;
		}

		if (path == null) {
			return;
		}

		if (currentWaypoint >= path.vectorPath.Count) {
			if(PathIsEnded){
				return;
			}

			//Debug.Log ("End of path reached.");
			PathIsEnded = true;
			return;
		}
		PathIsEnded = false;

		Vector3 dir = (path.vectorPath [currentWaypoint] - transform.position).normalized;
		dir *= speed * Time.fixedDeltaTime;

		// Set the animation for the direction the enemy is moving in
		m_Anim.SetFloat("Move", dir.x);

		// Move the AI
		rb.AddForce (dir, fMode);

		float dist = Vector3.Distance (transform.position, path.vectorPath [currentWaypoint]);

		if (dist < NextWaypointDistance) {
			currentWaypoint++;
			return;
		}
	}
}
