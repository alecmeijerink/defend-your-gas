﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public int scoreValue = 100;

	[System.Serializable]
	public class EnemyStats {
		public int Health = 100;
	}
	
	public EnemyStats stats = new EnemyStats();
	
	public void DamageEnemy(int damage){
		stats.Health -= damage;
		if (stats.Health <= 0) {
			//Debug.Log("Kill the enemy and add points to score");
			GameMaster.KillEnemy(this);
			ScoreManager.score += scoreValue;
		}
	}
	
	void Start(){
		//playerStats.Health -= 10;
	}
}
