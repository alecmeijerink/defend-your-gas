﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : MonoBehaviour {

	Slider healthSlider;
	public int fallBoundry = -20;
	//public int defaultHealth = 100;

	[System.Serializable]
	public class PlayerStats {
		public int Health = 100;
	}

	public PlayerStats playerStats = new PlayerStats();
	
	void Update (){
		if (transform.position.y <= fallBoundry) {
			DamagePlayer(99999999);
		}
	}

	void Start (){
//		GameObject temp = GameObject.Find("HealthSlider");
//		if (temp != null)
//		{
//			healthSlider = temp.GetComponent<Slider>();
//			if (healthSlider != null)
//			{
//				healthSlider.value = playerStats.Health;
//			}
//			else
//			{
//				Debug.LogError("[" + temp.name + "] - Does not contain a Slider Component!");
//			}
//			
//		}
//		else
//		{
//			Debug.LogError("Could not find an active GameObject named Volume Slider!");
//		}
	}

	public void DamagePlayer(int damage){
		playerStats.Health -= damage;
		if (playerStats.Health <= 0) {
			Debug.Log("Kill the player or balloon");
			GameMaster.KillPlayer(this);
		}
	}


}
