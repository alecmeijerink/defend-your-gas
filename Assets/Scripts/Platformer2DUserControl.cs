using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof (PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
        private PlatformerCharacter2D m_Character;

        private void Awake()
        {
            m_Character = GetComponent<PlatformerCharacter2D>();
        }

        private void Update()
        {
//            if (!m_Jump)
//            {
//                // Read the jump input in Update so button presses aren't missed.
//                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
//            }
        }

        private void FixedUpdate()
        {
            // Read the inputs.
			bool left = Input.GetKey("left");
			bool right = Input.GetKey("right");
			bool up = Input.GetKey("up");
			bool down = Input.GetKey("down");
            float h = CrossPlatformInputManager.GetAxis("Horizontal");

            // Pass all parameters to the character control script.
            m_Character.Move(h, left, right, up, down);
        }
    }
}
